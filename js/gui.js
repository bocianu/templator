const gui = () => {
	$('<ul/>').attr('id','menulist').addClass('menulist').appendTo('#menu');

	const addMenuItem = (name, handler, parent = 'menulist') => {
		$('<li/>').html(name).addClass('menuitem').appendTo(`#${parent}`)
    .bind('click', handler);
	}
	return {
		addMenuItem
	}
};
